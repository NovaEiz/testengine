using System.Collections;
using UnityEngine;

namespace TX5.Vehicle {

public static class Ext {
    public static T RandomItem<T>(this T[] array) {
        return array.Length > 0 ? array[Random.Range(0, array.Length)] : default;
    }
}

public class VehicleInputData {
    public float backwardThrotlle;

    public float brake;

    public float forwardThrottle;

    public bool jump;

    public float tilt;

    public void Clear() {
        tilt = 0f;
        forwardThrottle = 0f;
        backwardThrotlle = 0f;
        brake = 0f;
        jump = false;
    }

    public void Normalize() {
        tilt = Mathf.Clamp(tilt, -1f, 1f);
        forwardThrottle = Mathf.Clamp01(forwardThrottle);
        backwardThrotlle = Mathf.Clamp01(backwardThrotlle);
        brake = Mathf.Clamp01(brake);
    }
}

[RequireComponent(typeof(AudioSource))]
public class TestEngine : MonoBehaviour {
    public enum Mode {
        None,

        Idle,

        Throttle
    }

    [SerializeField] private float[] _throttleInit;

    [SerializeField] private float _throttleBegin;

    [SerializeField] private float _throttleEnd;

    [SerializeField] private float _idleInit;

    [SerializeField] private float _idleBegin;

    [SerializeField] private float _idleEnd;

    private float _loopBegin;

    private float _loopEnd;

    private Mode _mode = Mode.None;

    private float _pitch;

    private AudioSource _source;

    private VehicleInputData _vehicleInputData = new VehicleInputData();

    private void Awake() {
        _source = GetComponent<AudioSource>();
        _pitch = Random.Range(0.95f, 1.05f);
    }

    private void Start() {
        StartCoroutine(C_Pitch());
    }

    private void Update() {
        UpdateMode();
        if (_source.time >= _loopEnd) _source.time = _loopBegin;

        _source.pitch = Mathf.Lerp(_source.pitch, _pitch, Time.deltaTime);
    }

    private void UpdateMode() {
        // if (_dynamic.vehicleInputData == null) {
        //     _source.Stop();
        //     return;
        // };

        if (Input.GetKey(KeyCode.UpArrow)) {
            _vehicleInputData.forwardThrottle = 1;
        } else {
            _vehicleInputData.forwardThrottle = 0;
        }

        var newMode = _vehicleInputData.forwardThrottle <= 0 ? Mode.Idle : Mode.Throttle;
            // _vehicleInputData == null || _vehicleInputData.forwardThrottle <= 0 ||
            // Time.time - _dynamic.rearWheel.lastCollisionTime > 0.1f
            //     ? Mode.Idle
            //     : Mode.Throttle;

        if (_mode != newMode) {
            if (newMode == Mode.Idle) {
                _loopBegin = _idleBegin;
                _loopEnd = _idleEnd;
                _source.time = _idleInit;
            } else if (newMode == Mode.Throttle) {
                _loopBegin = _throttleBegin;
                _loopEnd = _throttleEnd;
                _source.time = _throttleInit.RandomItem();
            }

            _mode = newMode;
        }
    }

    private IEnumerator C_Pitch() {
        while (true) {
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            _pitch = Random.Range(0.8f, 1.1f);
            if (_mode == Mode.Throttle) {
                _source.time = _throttleInit.RandomItem();
            }
        }
    }
}

}